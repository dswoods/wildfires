<?php blade( 'frame/header.php'); ?>

<?php
    blade( 'component/header.php', ['header' => $header]);
 if(isset($message)) :
    blade( 'blade/component/message.php', compact('message', $message));
endif;?>

<?php
foreach ($fireLists as $key => $fireDetails):
    blade( 'component/fireDetails.php', $fireDetails);
    blade( 'component/spacer.php');
endforeach
    ?>
<?php blade( 'frame/footer.php'); ?>
