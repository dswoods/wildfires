<?php
?>
<div class="container-lg">
    <div class="row">
        <div class="col">
        <h3> <?= $Fire_Name ?></h3>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <strong>FPA ID:</strong>
        </div>
        <div class="col-10">
            <?= $FPA_ID ?>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <strong>Discovered:</strong>
        </div>
        <div class="col-10">
            <?= $Discovery_Date ?>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <strong>Cause:</strong>
        </div>
        <div class="col-10">
            <?= $Cause ?>
        </div>
    </div>
    
</div>
