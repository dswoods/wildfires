<?php
require '../vendor/autoload.php';
include_once '../global/utility/admin.php';
use App\SQLiteConnection;
initialize('router');

$router = new Router(new Request);


//==================================
// HOME
//==================================
$router->get('/', function($Request) {
    $page = new page();
  return $page->landing();
});
$router->get('/forestDetails', function($Request) {
    $page = new page();
  return $page->forestDetails();
});


 ?>
