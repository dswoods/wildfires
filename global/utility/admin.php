<?php


function globalActive(){
    echo 'globalActive';
}

function utilitySystems($filename){
    if(file_exists(dirname(__FILE__).'\../'.$filename)){
        include_once( dirname(__FILE__).'\../'.$filename);
    }
}

function blade($filename, $variable = array()){
    foreach ($variable as $key => $value) {
        ${$key} = $value;
    }
    if(file_exists(dirname(__FILE__).'\..\../public/resources/blade/'.$filename)){
        include( dirname(__FILE__).'\..\../public/resources/blade/'.$filename);
    }
}
function assets($filename){
    return dirname(__FILE__).'\..\../public/assets/'.$filename;

}


function initialize($type){
    $initialiseArray = array(
        'router' => array(
            'router/Request.php',
            'router/Router.php',
            'page/page.php',
        )
    );

    if(array_key_exists($type, $initialiseArray)){
        foreach ($initialiseArray[$type] as $key => $value) {
            utilitySystems($value);
        }
    }
}

?>
