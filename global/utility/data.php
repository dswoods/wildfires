<?php

use App\SQLiteConnection;

class data extends SQLite3{

    protected $filename, $forestID;
    function __construct()
    {
        $this->pdo = (new SQLiteConnection())->connect();
    }
// Set

// call
    public function forestlist()
    {
        return $this->getSites();
    }
    public function firelist($NWCG_REPORTING_UNIT_ID)
    {
        return $this->getFires($NWCG_REPORTING_UNIT_ID);
    }
    public function SiteName($NWCG_REPORTING_UNIT_ID)
    {
        return $this->getName($NWCG_REPORTING_UNIT_ID);
    }

// utility
    private function getName($NWCG_REPORTING_UNIT_ID) {
        $stmt = $this->pdo->query('SELECT NWCG_REPORTING_UNIT_NAME '
                . 'FROM Fires
                WHERE NWCG_REPORTING_UNIT_ID=
                :NWCG_REPORTING_UNIT_ID LIMIT 1;');
        $stmt->execute([':NWCG_REPORTING_UNIT_ID' => $NWCG_REPORTING_UNIT_ID]);
        $name = 'Not Found';
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $name = $row['NWCG_REPORTING_UNIT_NAME'];
        }
        return $name;
    }
    private function getSites() {
        $stmt = $this->pdo->query('SELECT
       COUNT(OBJECTID) FireCount, NWCG_REPORTING_UNIT_NAME , NWCG_REPORTING_UNIT_ID,
    	min(Fire_Year) ealiest,
	       max(Fire_Year) latest '
                . 'FROM Fires
                GROUP BY
                NWCG_REPORTING_UNIT_ID
                ORDER BY
                NWCG_REPORTING_UNIT_NAME ASC');
        $sites = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $sites[$row['NWCG_REPORTING_UNIT_ID']] = [
                'NWCGRUID' => $row['NWCG_REPORTING_UNIT_ID'],
                'FireCount' => $row['FireCount'],
                'NWCGRU_NAME' => $row['NWCG_REPORTING_UNIT_NAME'],
                'ealiest' => $row['ealiest'],
                'latest' => $row['latest']
            ];
        }
        return $sites;
    }
    private function getFires($NWCG_REPORTING_UNIT_ID) {
        $stmt = $this->pdo->query('SELECT FPA_ID, FIRE_NAME, date(DISCOVERY_DATE) DISCOVERY_DATE, STAT_CAUSE_DESCR  '
                . 'FROM Fires
                WHERE NWCG_REPORTING_UNIT_ID=
                :NWCG_REPORTING_UNIT_ID;
                ORDER BY
                DISCOVERY_DATE ASC');
        $stmt->execute([':NWCG_REPORTING_UNIT_ID' => $NWCG_REPORTING_UNIT_ID]);
        $fires = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {

            $fires[] = [
                'FPA_ID' => $row['FPA_ID'],
                'Fire_Name' => $row['FIRE_NAME'],
                'Discovery_Date' => $row['DISCOVERY_DATE'],
                'Cause' => $row['STAT_CAUSE_DESCR']
            ];
        }
        return $fires;
    }
    public function DD(){

      foreach (func_get_args() as $arg) {
        var_dump($arg);
      }
      exit();
    }
}
?>
