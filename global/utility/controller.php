<?php

class controller{

    function __construct()
    {
        echo 'controller <BR>';
    }
    public function viewBasic($file){
        include_once dirname(__FILE__).'\..\..\structure\view\\'.$file.'.php';
    }

    function view($filePath, $variables = array(), $print = true)
    {

        $output = NULL;
        if(file_exists(dirname(__FILE__).'\..\..\public\resources\view\\'.$filePath.'.php')){
            // Extract the variables to a local namespace
            extract($variables);

            // Start output buffering
            ob_start();

            // Include the template file
            include
            dirname(__FILE__).'\..\..\public\resources\view\\'.$filePath.'.php';

            // End buffering and return its contents
            $output = ob_get_clean();
        }
        if ($print) {
            print $output;
        }
        //return $output;

    }
}



?>
