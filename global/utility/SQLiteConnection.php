<?php
namespace App;

/**
 * SQLite connnection
 */
class SQLiteConnection {
    /**
     * PDO instance
     * @var type
     */
    private $pdo;

    /**
     * return in instance of the PDO object that connects to the SQLite database
     * @return \PDO
     */
    public function connect() {
        if ($this->pdo == null) {
        //$dir = file_exists('\..\../storage/'.$fname.'.sqlite');
            $this->pdo = new \PDO("sqlite:" . dirname(__FILE__).Config::PATH_TO_SQLITE_FILE);
        }
        return $this->pdo;
    }
}
