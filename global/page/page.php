<?php
utilitySystems('utility\controller.php');


class page extends controller{

    private $dir;

    function __construct()
    {
    }

    public function __call($name, $args)
    {
        $this->dir = dirname(__FILE__);
        if(strpos($name, '_')){
            $nameArray = explode('_', $name);
            $name = array_pop($nameArray);
            $this->dir .= '\\'.implode('\\', $nameArray);
        }
        $filename = $this->dir."\\".$name.'.php';
        if(file_exists($filename)){
            include_once $this->dir.'\\'.$name.'.php';
            $class = new $name();
            $page = $class->page();

        }else{
            echo "The file $filename does not exist";
        }

    }
    public function save($name)
    {
        $this->dir = dirname(__FILE__);
        if(strpos($name, '_')){
            $nameArray = explode('_', $name);
            $name = array_pop($nameArray);
            $this->dir .= '\\'.implode('\\', $nameArray);

        }
        $filename = $this->dir."\\".$name.'.php';
        if(file_exists($filename)){
            include_once $this->dir.'\\'.$name.'.php';
            $class = new $name();
            $page = $class->save();

        }else{
            echo "The file $filename does not exist";
        }
        return $this;
    }
    public function redirect($name)
    {
        header('Location: '.$name);
    }

}



?>
