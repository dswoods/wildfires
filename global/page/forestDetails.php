<?php

utilitySystems('utility\controller.php');
utilitySystems('utility\data.php');

class forestDetails extends controller{
    public $content =[];

    function __construct()
    {

    }
//=============================
// Page construct
//=============================
    public function page(){
        $this->data();
        $content = $this->content;
        $this->page = $this->view('forestDetails',
        $content);
    }
//=============================
// Data load
//=============================
    private function data(){

        $data = new data();
        $this->content['header'] = $data->SiteName($_GET["NWCGRUID"]);

        $this->content['fireLists'] = $data->firelist($_GET["NWCGRUID"]);

    }
}
?>
