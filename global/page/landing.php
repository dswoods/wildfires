<?php

utilitySystems('utility\controller.php');
utilitySystems('utility\data.php');

class landing extends controller{
    public $content =[];

    function __construct()
    {

    }
//=============================
// Page construct
//=============================
    public function page(){
        $this->data();
        $content = $this->content;
        $this->page = $this->view('landing',
        $content);
    }
//=============================
// Data load
//=============================
    private function data(){

        $data = new data();
        $this->content['header'] = 'Wildfire reports';
        $this->content['siteLists'] = $data->forestlist();
        // var_dump($this->content);
        // die();

    }
}
?>
